package com.eco.table

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.BeanPropertyRowMapper
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository
import java.sql.ResultSet


data class EP_USERS_SPRING(var vUserId:String ="",
                var vUserName: String = "",
                var vFirstName: String = "",
                var vLastName: String = "",
                var vEmail: String = "",
                var vCountryCode: String = "",
                var vGender: String = "",
                var vEnabled: String = "",
                var vEncrtedPassword: String =""
){
    fun getOid(@Autowired  jdbcTemplate: JdbcTemplate):String{
        var sOid = jdbcTemplate.queryForObject("SELECT MAX(USERID)+1 from EP_USERS_SPRING ", String::class.java).toString()
        println(">>>>>>sOid="+sOid)
        return sOid
    }
}

@Repository
class _EP_USERS_SPRING(@Autowired var jdbcTemplate: JdbcTemplate) {
    private val tableName = "EP_USERS_SPRING"

    fun addUser(userData: EP_USERS_SPRING) {
        var sOid :String
        sOid = EP_USERS_SPRING().getOid(jdbcTemplate)

        jdbcTemplate.update(
                "INSERT INTO EP_USERS_SPRING (USERID, USERNAME, FIRSTNAME, LASTNAME, EMAIL, " +
                        "COUNTRYCODE, GENDER, ENCRYTEDPASSWORD) "
                        + "VALUES (?,?,?,?,?,?,?,?)",
                sOid,
                userData.vUserName,
                userData.vFirstName,
                userData.vLastName,
                userData.vEmail,
                userData.vCountryCode,
                userData.vGender,
                userData.vEncrtedPassword
        )
    }

    fun queryAllusers(): List<EP_USERS_SPRING> = jdbcTemplate.query("SELECT * FROM EP_USERS_SPRING ORDER by USERID ",
            { rs: ResultSet, _: Int ->
                EP_USERS_SPRING(   rs.getString("USERID") ?:"",
                        rs.getString("USERNAME")?:"",
                        rs.getString("FIRSTNAME")?:"",
                        rs.getString("LASTNAME")?:"",
                        rs.getString("EMAIL")?:"",
                        rs.getString("COUNTRYCODE")?:"",
                        rs.getString("GENDER")?:"",
                        rs.getString("ENABLED")?:"",
                        rs.getString("ENCRYTEDPASSWORD")?:"")
            })

    fun deleteUsers(userData: EP_USERS_SPRING) {
        jdbcTemplate.update("Delete From EP_USERS_SPRING where USERID = ?",userData.vUserId)
    }

    fun editUser(userData: EP_USERS_SPRING) {
        jdbcTemplate.update(
                "UPDATE EP_USERS_SPRING set USERNAME=? ,FIRSTNAME=? ,LASTNAME=? ,EMAIL=? ,COUNTRYCODE=? " +
                        ",GENDER=? ,ENCRYTEDPASSWORD=?" +
                        "where USERID=?",
                userData.vUserName, userData.vFirstName, userData.vLastName,userData.vEmail,userData.vCountryCode,
                userData.vGender, userData.vEncrtedPassword,
                userData.vUserId
        )
    }

    fun listCheckUser(userData: EP_USERS_SPRING): List<Any> {
        var sUserName= userData.vUserName.trim().replace("--","")
        var sPassWord =userData.vEncrtedPassword.trim().replace("--","")
        println(">>>>>>sUserName="+sUserName)
        println(">>>>>>sPassWord="+sPassWord)
        return jdbcTemplate.query("SELECT * FROM EP_USERS_SPRING WHERE USERNAME='$sUserName' AND ENCRYTEDPASSWORD='$sPassWord'", arrayOf(), BeanPropertyRowMapper(Any::class.java))
    }


}
