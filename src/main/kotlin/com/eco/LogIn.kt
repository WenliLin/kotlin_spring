package com.eco

import com.eco.table.EP_USERS_SPRING
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import com.eco.unit.userUnit
import com.eco.util.DateUtil


@Controller
@RequestMapping("/")
class LogInController(@Autowired var indexService: userUnit) {

    @RequestMapping(method = arrayOf(RequestMethod.GET))
    fun doGet(model: Model, userData: EP_USERS_SPRING): String {
        return "home"
    }

    @RequestMapping(method = arrayOf(RequestMethod.POST))
    fun doPost(model: Model, userData: EP_USERS_SPRING): String {
        var mapCheckUser = indexService.listCheckUser(userData)
        if(mapCheckUser.size > 0){
            model.addAttribute("user", userData)

            var alUsers =indexService.allUsersList()
            model.addAttribute("users", alUsers)
            model.addAttribute("host", "http://localhost:8083/")
            model.addAttribute("getSysDatetime", DateUtil().getFormattedSysDate(DateUtil().DbDateTimeFormat))
            return "userList"
        }else{
            model.addAttribute("message", "User Name or Password is Fail")
            return "home"
        }
    }

}