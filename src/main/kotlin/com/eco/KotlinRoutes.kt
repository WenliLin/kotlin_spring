package com.eco

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.web.server.ErrorPage
import org.springframework.boot.web.servlet.error.ErrorController
//import org.springframework.cloud.gateway.route.Route
//import org.springframework.cloud.gateway.route.RouteLocator
//import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder
//import org.springframework.cloud.gateway.route.builder.filters
//import org.springframework.cloud.gateway.route.builder.routes
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
//import reactor.core.publisher.Flux
import java.util.function.Predicate
//import org.springframework.cloud.gateway.discovery.DiscoveryClientRouteDefinitionLocator
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit
import javax.servlet.http.HttpServletRequest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.web.servlet.error.ErrorAttributes
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.context.request.ServletRequestAttributes
import org.springframework.web.context.request.WebRequest

@Component
@ControllerAdvice
@RestController //springboot1.5.7版本,如果不加 这个会报错 jsonErrorHandler
class GlobalExceptionHandler {

    @RequestMapping("/hello")//http://localhost:8083/hello
    @Throws(Exception::class)
    fun hello() {
        println(">>ErrorHandler")
        throw  Exception("發生錯誤測試")
    }

    @Controller
    class UserController {
        @RequestMapping("/json")//http://localhost:8083/json
        @Throws(QkException::class)
        fun json(): String {
            throw QkException("發生錯誤 json")
        }
    }


    @ExceptionHandler(value = Exception::class)
    @Throws(Exception::class)
    fun defaultErrorHandler(req: HttpServletRequest, e: Exception): ModelAndView {
        println(">>defaultErrorHandler")
        val mav = ModelAndView()
        mav.addObject("exception", e)
        mav.addObject("url", req.requestURL)

        println(">>errorPage")
        mav.viewName = "errorPage"
        return mav
    }

    data class ErrorInfo<T>(var code: Int? = null,
                            var message: String? = "",
                            var url: String? = "",
                            var data: T? = null
    )

    class QkException(message: String) : Exception(message)

    @ExceptionHandler(value = QkException::class)
    @Throws(QkException::class)
    fun jsonErrorHandler(req: HttpServletRequest, e: QkException): ErrorInfo<String> {
        val r = ErrorInfo<String>()
        r.message = e.message
        r.code = 1
        r.data = "Some Data"
        r.url = req.requestURL.toString()
        return r
    }
}





@Configuration
@RestController
class KotlinRoutes {
//    @Bean
//    fun kotlinBasedRoutes(routeLocatorBuilder: RouteLocatorBuilder): RouteLocator =
//            routeLocatorBuilder.routes {
//                route {
//                    path("/**")
//                    println("123Routing")
//                    filters { stripPrefix(1) }
//                    uri("http://localhost:8083/")
//                }
//            }
//
//
//    fun gateway(routeLocator: RouteLocatorDsl.() -> Unit) = RouteLocatorDsl().apply(routeLocator).build()
//
//    class RouteLocatorDsl {
//        private val routes = mutableListOf<Route>()
//
//        /**
//         * DSL to add a route to the [RouteLocator]
//         *
//         * @see [Route.Builder]
//         */
//        fun route(id: String? = null, order: Int = 0, uri: String? = null, init: Route.Builder.() -> Unit) {
//            val builder = Route.builder()
//            if (uri != null) {
//                builder.uri(uri)
//            }
//            routes += builder.id(id).order(order).apply(init).build()
//        }
//
//        fun build(): RouteLocator {
//            return RouteLocator { Flux.fromIterable(this.routes) }
//        }
//
//        /**
//         * A helper to return a composed [Predicate] that tests against this [Predicate] AND the [other] predicate
//         */
//        infix fun <T> Predicate<T>.and(other: Predicate<T>) = this.and(other)
//
//        /**
//         * A helper to return a composed [Predicate] that tests against this [Predicate] OR the [other] predicate
//         */
//        infix fun <T> Predicate<T>.or(other: Predicate<T>) = this.or(other)
//    }
}