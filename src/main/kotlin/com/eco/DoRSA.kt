package com.eco

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import com.eco.util.RSAUtil

data class RSA_Data(var vPath:String ="",
                    var vOption:String="",
                    var vSource: String = "",
                    var vEncrypt: String = "",
                    var vDecrypt: String = "",
                    var vPassword : String = ""
)

@Controller
@RequestMapping("/RSA")
class DoRSA(@Autowired var indexService: IndexService) {

    @RequestMapping(method = arrayOf(RequestMethod.GET))
    fun doGet(model: Model, data: RSA_Data): String {
        return "doRSA"
    }

    @RequestMapping(method = arrayOf(RequestMethod.POST))
    fun doSubmit(model: Model, data: RSA_Data): String {
        println(">>>>vOption="+data.vOption)
        println(">>>>vPath="+data.vPath)
        println(">>>>vPassword="+data.vPassword)
        println(">>>>vEncrypt="+data.vEncrypt)

        var sEncode = ""
        if(data.vOption.equals("Encrypt")){
            sEncode = RSAUtil.doEncrypt(data.vPassword, data.vPath)
            model.addAttribute("vEncrypt", sEncode)
        }else{
            sEncode = RSAUtil.doDecrypt(data.vEncrypt, data.vPath)
            model.addAttribute("vPassword", sEncode)
        }
        model.addAttribute("message", sEncode)
        return "doRSA"
    }
}