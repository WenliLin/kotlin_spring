package com.eco.unit

import com.eco.table.EP_USERS_SPRING
import com.eco.table._EP_USERS_SPRING
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.ArrayList

@Service
class userUnit(@Autowired var indexRepository: _EP_USERS_SPRING) {

    fun addUser(userData: EP_USERS_SPRING) {
        indexRepository.addUser(userData)
    }

    fun allUsersList(): ArrayList<Any> {
        val ListUsers= indexRepository.queryAllusers()
        val alUsers = ArrayList<Any>()

        for (i in 0..ListUsers.size - 1) {
            val userData = ListUsers.get(i)
            val resultMap = HashMap<String, Any>()
            resultMap["vUserName"] = userData.vUserName
            resultMap["vFirstName"] = userData.vFirstName
            resultMap["vLastName"] = userData.vLastName
            resultMap["vEmail"] = userData.vEmail
            resultMap["vCountryCode"] = userData.vCountryCode
            resultMap["vGender"] = userData.vGender
            resultMap["vUserId"] = userData.vUserId
            resultMap["vEncrtedPassword"] = userData.vEncrtedPassword
            alUsers.add(i, resultMap)
        }
        return alUsers
    }

    fun deleteUsers(userData: EP_USERS_SPRING) {
        return indexRepository.deleteUsers(userData)
    }
    fun listCheckUser(userData: EP_USERS_SPRING) : List<Any> {
        return indexRepository.listCheckUser(userData)
    }
    fun editUser(userData: EP_USERS_SPRING) {
        return indexRepository.editUser(userData)
    }
}