package com.eco

import com.eco.table.EP_USERS_SPRING
import com.eco.unit.userUnit
import com.eco.util.DateUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping


@Controller
class UserListController(@Autowired var indexService: userUnit) {

    @RequestMapping("/doQuery")
    fun doQuery(model: Model, userData: EP_USERS_SPRING): String {
        println("doQuery")
        model.addAttribute("user", User())
        var alUsers =indexService.allUsersList()
        model.addAttribute("users", alUsers)
        model.addAttribute("host", "http://localhost:8083/")
        model.addAttribute("getSysDatetime", DateUtil().getFormattedSysDate(DateUtil().DbDateTimeFormat))

        return "userList"
    }

    @RequestMapping("/addUser")
    fun addUser(model: Model, userData: EP_USERS_SPRING): String {
        println(">>>>>>addUser ="+userData)
        indexService.addUser(userData)

        model.addAttribute("user", User())
        var alUsers =indexService.allUsersList()
        model.addAttribute("users", alUsers)
        model.addAttribute("host", "http://localhost:8083/")
        model.addAttribute("getSysDatetime", DateUtil().getFormattedSysDate(DateUtil().DbDateTimeFormat))

        return "userList"
    }

    @RequestMapping("/deleteUser")
    fun deleteUser(model: Model, userData: EP_USERS_SPRING): String {
        println(">>>>>>doDelete ="+userData.vUserId)
        indexService.deleteUsers(userData)

        model.addAttribute("user", User())
        var alUsers =indexService.allUsersList()
        model.addAttribute("users", alUsers)
        model.addAttribute("host", "http://localhost:8083/")
        model.addAttribute("getSysDatetime", DateUtil().getFormattedSysDate(DateUtil().DbDateTimeFormat))
        return "userList"
    }

    @RequestMapping("/editUser")
    fun editUser(model: Model, userData: EP_USERS_SPRING): String {
        println(">>>>>>editUser ="+userData)
        indexService.editUser(userData)

        model.addAttribute("user", User())
        var alUsers =indexService.allUsersList()
        model.addAttribute("users", alUsers)
        model.addAttribute("host", "http://localhost:8083/")
        model.addAttribute("getSysDatetime", DateUtil().getFormattedSysDate(DateUtil().DbDateTimeFormat))
        return "userList"
    }
}