package com.eco

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Controller
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import java.sql.ResultSet
import java.util.ArrayList
import org.springframework.jdbc.core.BeanPropertyRowMapper

//Define a data class that maps to both our
//form and database table
data class User(var vUserId:String ="",
                var vUserName: String = "",
                var vFirstName: String = "",
                var vLastName: String = "",
                var vEmail: String = "",
                var vCountryCode: String = "",
                var vGender: String = "",
                var vEnabled: String = "",
                var vEncrtedPassword: String =""
                )

@SpringBootApplication
class SpringJdbcApplication
    fun main(args: Array<String>) {
        SpringApplication.run(SpringJdbcApplication::class.java, *args)
    }

@Configuration
class Configuration {

    //First configure a data source that
    //generates an embedded db
//    @Bean(name = arrayOf("dataSource"))
//    fun dataSource(): DataSource {
//
//        //This will create a new embedded database and run the schema.sql script
//        return EmbeddedDatabaseBuilder()
//                .setType(EmbeddedDatabaseType.HSQL)
//                .addScript("schema.sql")
//                .build()
//    }
//
//    //Create a JdbcTemplate Bean that connects to our database
//    @Bean
//    fun jdbcTemplate(@Qualifier("dataSource") dataSource: DataSource): JdbcTemplate {
//        return JdbcTemplate(dataSource)
//    }
}

@Controller
@RequestMapping("/user")
class IndexController(@Autowired var indexService: IndexService) {

    @RequestMapping(method = arrayOf(RequestMethod.GET))
    fun doGet(model: Model, user: User): String {
        println(">>>>>>doGet ")
        model.addAttribute("user", User())
        println(">>>>>>doGet MM")
        val list =indexService.allUsers()


        val alUsers = ArrayList<Any>()
        val alUsersmap = ArrayList<User>()
        val vMap: HashMap<Int, String> = hashMapOf()
        for (i in 0..list.size-1) {
            val user = list.get(i)
            val resultMap = HashMap<String, Any>()
            resultMap["vUserName"] = user.vUserName
            resultMap["vFirstName"] = user.vFirstName
            resultMap["vLastName"] = user.vLastName
            resultMap["vEmail"] = user.vEmail
            resultMap["vCountryCode"] = user.vCountryCode
            resultMap["vGender"] = user.vGender
            resultMap["vUserId"] = user.vUserId
            alUsers.add(i,resultMap)
        }
        model.addAttribute("users", alUsers)


        println(">>>>>list="+list )
        println(">>>>>alUsers="+alUsers)
        println(">>>>>>doGet END")
        model.addAttribute("host", "http://localhost:8083/")
        return "index"
    }

    @RequestMapping(method = arrayOf(RequestMethod.POST))
    fun doPost(model: Model, user: User): String {
        println(">>>>>>doPost ")
        indexService.addUser(user)

        model.addAttribute("user", User())
        println(">>>>>>doPost MM")
        model.addAttribute("allUsers", indexService.allUsers())
        model.addAttribute("host", "http://localhost:8083/")
        println(">>>>>>doPost END")
        return "index"
    }
    @RequestMapping(method = arrayOf(RequestMethod.DELETE))
    fun DeleteUser(model: Model, user: User): String {
        println(">>>>>>doDelete ")
        indexService.deleteUsers(user)

        model.addAttribute("user", User())
        println(">>>>>>doDelete MM")
        model.addAttribute("allUsers", indexService.allUsers())
        model.addAttribute("host", "http://localhost:8083/")
        println(">>>>>>doDelete END")
        return "index"
    }

    @RequestMapping(method = arrayOf(RequestMethod.PATCH))
    fun editUser(model: Model, user: User): String {
        println(">>>>>>editUser ")
        indexService.deleteUsers(user)

        model.addAttribute("user", User())
        println(">>>>>>editUser MM")
        model.addAttribute("allUsers", indexService.allUsers())
        model.addAttribute("host", "http://localhost:8083/")
        println(">>>>>>editUser END")
        return "index"
    }
}

@Service
class IndexService(@Autowired var indexRepository: IndexRepository) {

    fun addUser(user: User) {
        indexRepository.addUser(user)
    }

    fun checkUsers(user: User): Map<String,Any> {//cant null
        println(user.vUserName+">>>>"+user.vEncrtedPassword)

        return indexRepository.checkUsers(user)
    }

    fun allUsers(): List<User> {
        return indexRepository.allUsers()
    }

    fun allUsersList():  ArrayList<Any> {
        val ListUsers= indexRepository.allUsers()
        val alUsers = ArrayList<Any>()
        for (i in 0..ListUsers.size - 1) {
            val user = ListUsers.get(i)
            val resultMap = HashMap<String, Any>()
            resultMap["vUserName"] = user.vUserName
            resultMap["vFirstName"] = user.vFirstName
            resultMap["vLastName"] = user.vLastName
            resultMap["vEmail"] = user.vEmail
            resultMap["vCountryCode"] = user.vCountryCode
            resultMap["vGender"] = user.vGender
            resultMap["vUserId"] = user.vUserId
            resultMap["vUserId"] = user.vUserId
            alUsers.add(i, resultMap)
        }
        return alUsers
    }

    fun deleteUsers(user: User) {
        return indexRepository.deleteUsers(user)
    }
    fun listCheckUser(user: User) : List<Any> {
        return indexRepository.listCheckUser(user)
    }
    fun editUser(user: User) {
        return indexRepository.editUser(user)
    }
}

@Repository
class IndexRepository(@Autowired var jdbcTemplate: JdbcTemplate) {

    fun addUser(user: User) {
        //We can use SimpleJdbcInsert to insert a value into our table
        //The becomes super concise when combined with Kotlins apply and mapOf functions
        var sOid :String
        sOid = jdbcTemplate.queryForObject("SELECT MAX(USERID)+1 from EP_USERS_SPRING ", String::class.java).toString()
        jdbcTemplate.update(
                "INSERT INTO EP_USERS_SPRING (USERID, USERNAME, FIRSTNAME, LASTNAME, EMAIL, COUNTRYCODE, GENDER) VALUES (?,?,?,?,?,?,?)",
                sOid,user.vUserName, user.vFirstName, user.vLastName, user.vEmail, user.vCountryCode, user.vGender
        )
    }

    //This allows us to query the Users table and return a list of users
    //This is one method call to jdbc Template with a lambda expression which makes the code
    //incredibly concise
    fun allUsers(): List<User> = jdbcTemplate.query("SELECT * FROM EP_USERS_SPRING",
            { rs: ResultSet, _: Int ->
                User(   rs.getString("USERID") ?:"",
                        rs.getString("USERNAME")?:"",
                        rs.getString("FIRSTNAME")?:"",
                        rs.getString("LASTNAME")?:"",
                        rs.getString("EMAIL")?:"",
                        rs.getString("COUNTRYCODE")?:"",
                        rs.getString("GENDER")?:"",
                        rs.getString("ENABLED")?:"",
                        rs.getString("ENCRYTEDPASSWORD")?:"")
            })

    fun deleteUsers(user: User) {
        jdbcTemplate.update("Delete From EP_USERS_SPRING where USERID = ?",user.vUserId)
    }

    fun editUser(user: User) {
        jdbcTemplate.update(
                "UPDATE EP_USERS_SPRING set USERNAME=? ,FIRSTNAME=? ,LASTNAME=? ,EMAIL=? ,COUNTRYCODE=? ,GENDER=? where USERID=?",
                user.vUserId,user.vUserName, user.vFirstName, user.vLastName, user.vEmail, user.vCountryCode, user.vGender,
                user.vUserId
        )
    }

    fun checkUsers(user: User): Map<String,Any> {
//        val mapList: Map<String, Any>
//        mapList = jdbcTemplate.queryForMap("SELECT * FROM EP_USERS_SPRING WHERE USERNAME=? AND ENCRYTEDPASSWORD=?",user.vUserName,user.vEncrtedPassword)

        val list = jdbcTemplate.queryForMap("SELECT * FROM EP_USERS_SPRING WHERE USERNAME=? AND ENCRYTEDPASSWORD=?",user.vUserName,user.vEncrtedPassword)


        return if (list != null && list.size > 0) {
            return list
        } else {
            return  list
        }
//        return mapList
    }
    fun listCheckUser(user: User): List<Any> {
        var sUserName= user.vUserName.trim().replace("--","")
        var sPassWord =user.vEncrtedPassword.trim().replace("--","")
        return jdbcTemplate.query("SELECT * FROM EP_USERS_SPRING WHERE USERNAME='$sUserName' AND ENCRYTEDPASSWORD='$sPassWord'", arrayOf(), BeanPropertyRowMapper(Any::class.java))
    }


}


