package com.eco

import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.stereotype.Component
import javax.servlet.ServletException
import java.io.IOException
import org.springframework.util.StringUtils.getFilename
import org.tuckey.web.filters.urlrewrite.Conf
import javax.servlet.FilterConfig
import org.tuckey.web.filters.urlrewrite.UrlRewriteFilter


@Component
class WsdlUrlRewriteFilter : UrlRewriteFilter() {
    //http://XXXX/rewrite-status
    @Value("classpath:/urlrewrite.xml")
    private val resource: Resource? = null

    @Throws(ServletException::class)
    override fun loadUrlRewriter(filterConfig: FilterConfig?) {
        try {
            val conf = Conf(filterConfig!!.servletContext, resource!!.getInputStream(), resource!!.getFilename(), "")
            checkConf(conf)
        } catch (ex: IOException) {
            throw ServletException("Unable to load URL-rewrite configuration file from $CONFIG_LOCATION", ex)
        }
    }
    companion object {

        private val CONFIG_LOCATION = "classpath:/urlrewrite.xml"
    }
}