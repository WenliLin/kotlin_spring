package com.eco.util

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.text.SimpleDateFormat

public class DateUtil {
    var vSystem = LocalDateTime.now()
    var DbDateFormat = "yyyyMMdd"

    var DbDateTimeFormat = "yyyyMMddHHmmss"

    var DbDateEmpty = ""

    var EdiDateTimeFormat = "yyyyMMddHHmmssSSS"

    var mailPerMonthFormat = "yyyy-mm"

    var UiDateFormat = "yyyy/MM/dd"

    var UiDateTimeFormat = "yyyy/MM/dd HH:mm"

    var UiTimeStampFormat = "yyyy/MM/dd HH:mm:ss"

    var UiTimeFormat = "HH:mm"

    var DbTimeFormat = "HHmm" // mister

    var UiEmptyDisplay = "--"

    var ErpDateFormat = "yyyy-MM-dd"

    var ErpDateTimeFormat = "yyyy-MM-dd HH:mm:ss"

    // public static String ErpDateTimeFormat = "yyyy-MM-dd HH:mm:ss.S";
    var ErpTimeStampFormat = "yyyy-MM-dd HH:mm:ss.S"

    var ErpDateTimeFormat2 = "MM/dd/yyyy HH:mm:ss"

    var WsDateFormat = "yyyy/MM/dd"

    var WsDateTimeFormat = "yyyy/MM/dd HH:mm:ss"

    var WsTimeStampFormat = "yyyy/MM/dd HH:mm:ss"

    var DbDateZeroTimeFormat = "yyyyMMdd000000"

    var DbDateEndTimeFormat = "yyyyMMdd999999"

    var DateTimeForNoon = "yyyyMMdd120000"

    var CTDateTimeFormat = "yyyy/MM/dd HH:mm:ss"

    var CTDateFormat = "yyyy/MM/dd"

    var DbEmptyDisplay = ""

    var ErpEmptyDisplay = ""

    var javaSqlDateFormat = "yyyy-MM-dd"

    var javaSqlTimeFormat = "HH:mm:ss"

    var printDateFormat = "dd/MMM/yyyy"

    var printDateFormat2 = "dd-MMM-yyyy"

    var printDateFormat3 = "dd/MMM/yy"

    var DateFormat = "yyyy.MM.dd"

    var DateNoYear = "MM/dd"

    var YearWkFormat = "yyyy'WK'w"

    var SqlDateFormat = "yyyyMMddHH24miss"

    var monthFormat = "yyyyMM"

    var UiMonthFormat = "yyyy/MM/"

    var DbDateTimeFormatNoSec = "yyyyMMddHHmm00"

    var DbDateTimeFormatNoMinSec = "yyyyMMddHH0000"
    public fun getSysDateTimes():Any{
        val formatter = DateTimeFormatter.ofPattern(UiTimeStampFormat)
        val formatted = vSystem.format(formatter)
        return formatted
    }

    public fun getFormattedSysDate(sFormate : String): Any {
        val formatter = DateTimeFormatter.ofPattern(sFormate)
        val formatted = vSystem.format(formatter)
        return formatted
    }

}
