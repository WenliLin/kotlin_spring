package com.eco.util;

import java.io.*;
import java.nio.file.Paths;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;

import org.apache.axis.encoding.Base64;

/** */
/**
 * <p>
 * RSA公開金鑰/私密金鑰/簽名工具包
 * </p>
 * <p>
 * 羅奈爾得·李維斯特（Ron [R]ivest）、阿迪·薩莫爾（Adi [S]hamir）和倫納德·阿德曼（Leonard [A]dleman）
 * </p>
 * <p>
 * 字串格式的金鑰在未在特殊說明情況下都為BASE64編碼格式<br/>
 * 由於非對稱加密速度極其緩慢，一般檔不使用它來加密而是使用對稱加密，<br/>
 * 非對稱加密演算法可以用來對對稱加密的金鑰加密，這樣保證金鑰的安全也就保證了資料的安全
 * </p>
 *
 * @author IceWee JUN
 * @date 2012-4-26
 * @version 1.0
 */

public class RSAUtil {

	/** */
	/**
	 * 加密演算法RSA
	 */
	public static final String KEY_ALGORITHM = "RSA";

	/** */
	/**
	 * 簽名演算法
	 */
	public static final String SIGNATURE_ALGORITHM = "MD5withRSA";

	/** */
	/**
	 * 獲取公開金鑰的key
	 */
	static final String PUBLIC_KEY = "RSAPublicKey";

	/** */
	/**
	 * 獲取私密金鑰的key
	 */
	static final String PRIVATE_KEY = "RSAPrivateKey";

	/** */
	/**
	 * RSA最大加密明文大小
	 */
	private static final int MAX_ENCRYPT_BLOCK = 117;

	/** */
	/**
	 * RSA最大解密密文大小
	 */
	private static final int MAX_DECRYPT_BLOCK = 128;

	/** */
	/**
	 * <p>
	 * 生成金鑰對(公開金鑰和私密金鑰) 檔案
	 * </p>
	 *
	 * @return
	 * @throws Exception
	 */
	public static Map<String, Object> genKeyPair(String path) throws Exception {
		KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(KEY_ALGORITHM);
		SecureRandom random = new SecureRandom();
		random.setSeed("eco26536999".getBytes());
		keyPairGen.initialize(1024, random);
		KeyPair keyPair = keyPairGen.generateKeyPair();
		RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
		RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();

		RSAUtil.saveKeyPair(path, keyPair);

		Map<String, Object> keyMap = new HashMap<String, Object>(2);
		keyMap.put(PUBLIC_KEY, publicKey);
		keyMap.put(PRIVATE_KEY, privateKey);
		return keyMap;
	}

	/** */
	/**
	 * <p>
	 * 用私密金鑰對資訊生成數位簽章
	 * </p>
	 *
	 * @param data
	 *            已加密資料
	 * @param privateKey
	 *            私密金鑰(BASE64編碼)
	 *
	 * @return
	 * @throws Exception
	 */
	public static String sign(byte[] data, String privateKey) throws Exception {
		byte[] keyBytes = decode(privateKey);
		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		PrivateKey privateK = keyFactory.generatePrivate(pkcs8KeySpec);
		Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
		signature.initSign(privateK);
		signature.update(data);
		return encode(signature.sign());
	}

	/** */
	/**
	 * <p>
	 * 校驗數位簽章
	 * </p>
	 *
	 * @param data
	 *            已加密資料
	 * @param publicKey
	 *            公開金鑰(BASE64編碼)
	 * @param sign
	 *            數位簽章
	 *
	 * @return
	 * @throws Exception
	 *
	 */
	public static boolean verify(byte[] data, String publicKey, String sign)
			throws Exception {
		byte[] keyBytes = decode(publicKey);
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		PublicKey publicK = keyFactory.generatePublic(keySpec);
		Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
		signature.initVerify(publicK);
		signature.update(data);
		return signature.verify(decode(sign));
	}

	/** */
	/**
	 * <P>
	 * 私密金鑰解密
	 * </p>
	 *
	 * @param encryptedData
	 *            已加密資料
	 * @param privateKey
	 *            私密金鑰(BASE64編碼)
	 * @return
	 * @throws Exception
	 */
	public static byte[] decryptByPrivateKey(byte[] encryptedData, String privateKey)
			throws Exception {
		byte[] keyBytes = decode(privateKey);
		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key privateK = keyFactory.generatePrivate(pkcs8KeySpec);
		// Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");

		cipher.init(Cipher.DECRYPT_MODE, privateK);
		int inputLen = encryptedData.length;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] cache;
		int i = 0;
		// 對資料分段解密
		while (inputLen - offSet > 0) {
			if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
				cache = cipher.doFinal(encryptedData, offSet, MAX_DECRYPT_BLOCK);
			} else {
				cache = cipher.doFinal(encryptedData, offSet, inputLen - offSet);
			}
			out.write(cache, 0, cache.length);
			i++;
			offSet = i * MAX_DECRYPT_BLOCK;
		}
		byte[] decryptedData = out.toByteArray();
		out.close();
		return decryptedData;
	}

	/** */
	/**
	 * <p>
	 * 公開金鑰解密
	 * </p>
	 *
	 * @param encryptedData
	 *            已加密資料
	 * @param publicKey
	 *            公開金鑰(BASE64編碼)
	 * @return
	 * @throws Exception
	 */
	public static byte[] decryptByPublicKey(byte[] encryptedData, String publicKey)
			throws Exception {
		byte[] keyBytes = decode(publicKey);
		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key publicK = keyFactory.generatePublic(x509KeySpec);
		// Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE, publicK);
		int inputLen = encryptedData.length;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] cache;
		int i = 0;
		// 對資料分段解密
		while (inputLen - offSet > 0) {
			if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
				cache = cipher.doFinal(encryptedData, offSet, MAX_DECRYPT_BLOCK);
			} else {
				cache = cipher.doFinal(encryptedData, offSet, inputLen - offSet);
			}
			out.write(cache, 0, cache.length);
			i++;
			offSet = i * MAX_DECRYPT_BLOCK;
		}
		byte[] decryptedData = out.toByteArray();
		out.close();
		return decryptedData;
	}

	/** */
	/**
	 * <p>
	 * 公開金鑰加密
	 * </p>
	 *
	 * @param data
	 *            來源資料
	 * @param publicKey
	 *            公開金鑰(BASE64編碼)
	 * @return
	 * @throws Exception
	 */
	public static byte[] encryptByPublicKey(byte[] data, String publicKey)
			throws Exception {
		byte[] keyBytes = decode(publicKey);
		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key publicK = keyFactory.generatePublic(x509KeySpec);
		// 對資料加密
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		// Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.ENCRYPT_MODE, publicK);
		int inputLen = data.length;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] cache;
		int i = 0;
		// 對資料分段加密
		while (inputLen - offSet > 0) {
			if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
				cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
			} else {
				cache = cipher.doFinal(data, offSet, inputLen - offSet);
			}
			out.write(cache, 0, cache.length);
			i++;
			offSet = i * MAX_ENCRYPT_BLOCK;
		}
		byte[] encryptedData = out.toByteArray();
		out.close();
		return encryptedData;
	}

	/** */
	/**
	 * <p>
	 * 私密金鑰加密
	 * </p>
	 *
	 * @param data
	 *            來源資料
	 * @param privateKey
	 *            私密金鑰(BASE64編碼)
	 * @return
	 * @throws Exception
	 */
	public static byte[] encryptByPrivateKey(byte[] data, String privateKey)
			throws Exception {
		byte[] keyBytes = decode(privateKey);
		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key privateK = keyFactory.generatePrivate(pkcs8KeySpec);
		// Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.ENCRYPT_MODE, privateK);
		int inputLen = data.length;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] cache;
		int i = 0;
		// 對資料分段加密
		while (inputLen - offSet > 0) {
			if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
				cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
			} else {
				cache = cipher.doFinal(data, offSet, inputLen - offSet);
			}
			out.write(cache, 0, cache.length);
			i++;
			offSet = i * MAX_ENCRYPT_BLOCK;
		}
		byte[] encryptedData = out.toByteArray();
		out.close();
		return encryptedData;
	}

	/** */
	/**
	 * <p>
	 * 獲取私密金鑰
	 * </p>
	 *
	 * @param keyMap
	 *            金鑰對
	 * @return
	 * @throws Exception
	 */
	public static String getPrivateKey(Map<String, Object> keyMap)
			throws Exception {
		Key key = (Key) keyMap.get(PRIVATE_KEY);
		return encode(key.getEncoded());
	}

	/** */
	/**
	 * <p>
	 * 獲取公開金鑰
	 * </p>
	 *
	 * @param keyMap
	 *            金鑰對
	 * @return
	 * @throws Exception
	 */
	public static String getPublicKey(Map<String, Object> keyMap)
			throws Exception {
		Key key = (Key) keyMap.get(PUBLIC_KEY);
		return encode(key.getEncoded());
	}

	/** */
	/**
	 * <p>
	 * Base64 編碼
	 * </p>
	 *
	 * @param bytes
	 * @return String
	 * @throws Exception
	 */
	public static String encode(byte[] bytes) throws Exception {
		final String encodedText = Base64.encode(bytes);
		return encodedText;
	}

	/** */
	/**
	 * <p>
	 * Base64 解碼
	 * </p>
	 * @param sBase64
	 * @return byte
	 * @throws Exception
	 */
	public static byte[] decode(String sBase64) throws Exception {
		return Base64.decode(sBase64);
	}

	/** */
	/**
	 * <p>
	 * 儲存 private.key/public.key
	 * </p>
	 *
	 * @param path,
	 *            KeyPair
	 * @throws IOException
	 */
	public static void saveKeyPair(String path, KeyPair keyPair) throws IOException {
		PrivateKey privateKey = keyPair.getPrivate();
		PublicKey publicKey = keyPair.getPublic();

		// Store Public Key.
		File fileForPublicKey = Paths.get(path, "public.key").toFile();
		System.out.println("Public key will be output to '" + fileForPublicKey + "'");

		X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(publicKey.getEncoded());
		try (FileOutputStream fos = new FileOutputStream(fileForPublicKey)) {
			fos.write(x509EncodedKeySpec.getEncoded());
		}

		// Store Private Key.
		File fileForPrivateKey = Paths.get(path, "private.key").toFile();
		System.out.println("Private key will be output to '" + fileForPrivateKey + "'");

		PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(privateKey.getEncoded());
		try (FileOutputStream fos = new FileOutputStream(fileForPrivateKey)) {
			fos.write(pkcs8EncodedKeySpec.getEncoded());
		}
	}

	/** */
	/**
	 * <p>
	 * 讀取 private.key，產生PRIVATE_KEY
	 * </p>
	 *
	 * @return Map
	 * @throws Exception
	 */
	public static Map<String, Object> loadFilePrivateKey(String sPath, String sKeyName) throws Exception {
		File fileForPrivateKey = Paths.get(sPath, sKeyName).toFile();
		System.out.println(">>>fileForPrivateKey:" + fileForPrivateKey);
		PrivateKey privateKey = null;

		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		try (FileInputStream fis = new FileInputStream(fileForPrivateKey)) {
			byte[] loadedBytes = new byte[(int) fileForPrivateKey.length()];
			fis.read(loadedBytes);

			PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(loadedBytes);
			privateKey = keyFactory.generatePrivate(privateKeySpec);
		}

		Map<String, Object> keyMap = new HashMap<String, Object>(2);
		keyMap.put(PRIVATE_KEY, privateKey);
		return keyMap;
	}

	/** */
	/**
	 * <p>
	 * 讀取 public.key，產生PUBLIC_KEY
	 * </p>
	 *
	 * @return Map
	 * @throws Exception
	 */
	public static Map<String, Object> loadFilePublicKey(String sPath, String sKeyName) throws Exception {
		File fileForPublicKey = Paths.get(sPath, sKeyName).toFile();
		System.out.println(">>>fileForPublicKey:" + fileForPublicKey);
		PublicKey publicKey = null;

		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		try (FileInputStream fis = new FileInputStream(fileForPublicKey)) {
			byte[] loadedBytes = new byte[(int) fileForPublicKey.length()];
			fis.read(loadedBytes);

			X509EncodedKeySpec spec = new X509EncodedKeySpec(loadedBytes);
			publicKey = keyFactory.generatePublic(spec);
		}

		Map<String, Object> keyMap = new HashMap<String, Object>(2);
		keyMap.put(PUBLIC_KEY, publicKey);
		return keyMap;
	}


	public static String doEncrypt(String sSource, String sPath) throws Exception {
		String sEncode="";
		try {
			byte[] data = sSource.getBytes();
			Map<String, Object> MapKeyNew = loadFilePublicKey(sPath, "public.key");
			String DEFAULT_PUBLIC_KEY = getPublicKey(MapKeyNew);
			byte[] encodedData = encryptByPublicKey(data, DEFAULT_PUBLIC_KEY);
			System.out.println(">>>加密后文字=" + encode(encodedData));

			sEncode= encode(encodedData);

//			FileWriter fw = new FileWriter(sPath + "/encodedData.txt");
//			fw.write(encode(encodedData));
//			fw.flush();
//			fw.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return sEncode;
	}

	public static String doDecrypt(String sSource,String sPath) throws Exception {
		String sEncode="";
		try {
//			byte[] data = sSource.getBytes();
//			File file = new File(sPath + "/encodedData.txt");
//			FileInputStream readIn = new FileInputStream(file);
//			InputStreamReader read = new InputStreamReader(readIn, "utf-8");
//			BufferedReader bufferedReader = new BufferedReader(read);
//			String oneLine = null;
//			String sEncodedData = "";
//			while ((oneLine = bufferedReader.readLine()) != null) {
//				sEncodedData = oneLine;
//			}
//			read.close();
//			System.out.println(">>>sEncodedData=" + sEncodedData);

			Map<String, Object> MapKeyNew = loadFilePrivateKey(sPath, "private.key");
			String DEFAULT_PRIVATE_KEY = getPrivateKey(MapKeyNew);
			byte[] byEncodedData = decode(sSource);
			byte[] byDecodedData = decryptByPrivateKey(byEncodedData,
					DEFAULT_PRIVATE_KEY);
			String sPassWord = new String(byDecodedData);
			System.out.println(">>>解密后文字=" + sPassWord);

			sEncode= sPassWord;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sEncode;
	}

}
