/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: ZH (Chinese; 中文 (Zhōngwén), 汉语, 漢語)
 * Region: TW (Taiwan)
 */
$.extend( $.validator.messages, {
	required: "<font color='Red' >必須填寫</font>",
	remote: "<font color='Red' >請修正此欄位</font>",
	email: "<font color='Red' >請輸入有效的電子郵件</font>",
	url: "<font color='Red' >請輸入有效的網址</font>",
	date: "<font color='Red' >請輸入有效的日期</font>",
	dateISO: "<font color='Red' >請輸入有效的日期 (YYYY-MM-DD)</font>",
	number: "<font color='Red' >請輸入正確的數值</font>",
	digits: "<font color='Red' >只可輸入數字</font>",
	creditcard: "<font color='Red' >請輸入有效的信用卡號碼</font>",
	equalTo: "<font color='Red' >請重複輸入一次</font>",
	extension: "<font color='Red' >請輸入有效的後綴</font>",
	maxlength: $.validator.format( "<font color='Red' >最多 {0} 個字</font>" ),
	minlength: $.validator.format( "<font color='Red' >最少 {0} 個字</font>" ),
	rangelength: $.validator.format( "<font color='Red' >請輸入長度為 {0} 至 {1} 之間的字串</font>" ),
	range: $.validator.format( "<font color='Red' >請輸入 {0} 至 {1} 之間的數值</font>" ),
	max: $.validator.format( "<font color='Red' >請輸入不大於 {0} 的數值</font>" ),
	min: $.validator.format( "<font color='Red' >請輸入不小於 {0} 的數值</font>" )
} );
