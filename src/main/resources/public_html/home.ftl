<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8"></meta>

    <title>Kotlin_SpringMVC</title>

    <script
            src="https://code.jquery.com/jquery-3.2.1.js"
            integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
            crossorigin="anonymous" >
    </script>

    <!-- Latest compiled and minified CSS & JS -->
    <link rel="stylesheet" media="screen" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"></link>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script language="javascript">
            function doLogin(){               
                $("#loginForm").submit();
            }   
    </script>
</head>
<body>
<div class="jumbotron">
    <div class="container">
        <h1>Login User</h1>
        <p>Demonstrates Reading/Writing from Database using Spring JDBC Template and Kotlin</p>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <form id="loginForm" method="post" role="form" th:object="${user}">
                    <div class="form-group">
                        <label for="Email">User Name</label>
                        <input type="text" class="form-control" name="vUserName"
                               id="vUserName" placeholder="Email Address or ID " value="${vUserName!''}"></input>
                    </div>

                    <div class="form-group">
                        <label for="CountryCode">PassWord</label>
                        <input type="password" class="form-control" name="vEncrtedPassword"
                               id="vEncrtedPassword" placeholder="XXXXX" value="${vEncrtedPassword!''}"></input>
                    </div>
                        <button type="button"  class="btn btn-primary" onclick="javascript:doLogin();">Login</button>
                    <tr>
                        <td><font color="Red" >${message!''}</font></td>
                    </tr>

                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>