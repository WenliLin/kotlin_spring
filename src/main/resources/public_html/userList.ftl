<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">

<head>
    <meta charset="UTF-8"></meta>
    <title>Title</title>
    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS & JS -->
    <link rel="stylesheet" media="screen" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"></link>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <#assign path="${request.getContextPath()}">
 <!--    <script type="text/javascript"  src="${path}/js/jquery.validate-1.17.0.js?v=${getSysDatetime!''}"></script>
    <script type="text/javascript"  src="${path}/js/jquery.validate.messages_zh_TW.js?v=${getSysDatetime!''}"></script>  -->
        <script language="javascript">
          <#include "js/jquery.validate-1.17.0.js" >
          <#include "js/jquery.validate.messages_zh_TW.js" >
        </script>
        <!--Web <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js?v=${getSysDatetime!''}"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js?v=${getSysDatetime!''}"></script> -->
        <script language="javascript">

        function doEdit(vUserId, vUserName, vFirstName, vLastName, vEmail, vCountryCode, vGender) {
          document.getElementById("addUser").style.display = '';
          document.getElementById("editSubmit").style.display = '';
          document.getElementById("newSubmit").style.display = 'none';
          document.getElementById("showPassWord").style.display = 'none';

          document.getElementById("vUserId").value = vUserId;
          document.getElementById("vUserName").value = vUserName;
          document.getElementById("vFirstName").value = vFirstName;
          document.getElementById("vLastName").value = vLastName;
          document.getElementById("vEmail").value = vEmail;
          document.getElementById("vCountryCode").value = vCountryCode;
          document.getElementById("vGender").value = vGender;
        }

        function doOpenAdd() {
          document.getElementById("addUser").style.display = '';
        }

        function doEditUser() {
            if (confirm('Are you sure edit?')) {
                $("form").submit(function ()  {  
                    if  ($("#addUser").valid())  {     /*驗證成功，post the form data to server*/   } 
                    else  return  false;
                });
                document.getElementById("addUser").action = "/editUser";

                $("#addUser").submit();
            }

        }

        function doAddUser() {
            $("form").submit(function ()  {  
                if  ($("#addUser").valid())  {     /*驗證成功，post the form data to server*/   } 
                else  return  false;
            });

            $("#addUser").submit();
        }

        function doQuery(){
          document.getElementById("addUser").action = "/Query";
          $("#addUser").submit();

        }

        function doDelete(vUserId, vUserName) {
            if (confirm('Are you sure delete UserName:' + vUserName + ' ?')) {
              document.getElementById("vUserId").value = vUserId;
              document.getElementById("addUser").action = "/delete";
              $("#addUser").submit();
            }
        }
        $(document).ready(function() {

            
        });
        </script>
</head>

<body>
    <div class="jumbotron">
        <div class="container">
            <h1>User List</h1>
            <p>Demonstrates CRUD(Create/Retrieve/Update/Delete) from Database using Spring JDBC Template and Kotlin</p>
            <h4>本機：${host!''} </h4>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <form action="/addUser" id="addUser" method="POST" role="form" class="cmxform" th:object="${user.vUserId}" style="display: none;">
                        <legend>Insert User</legend>
                        <div class="form-group">
                            <TD>User Name</TD>
                            <input type="text" class="form-control" name="vUserName" id="vUserName" placeholder="Use rName" value="${vUserName!''}" required></input>
                        </div>
                        <div class="form-group">
                            <TD>First Name</TD>
                            <input type="text" class="form-control" name="vFirstName" id="vFirstName" placeholder="First Name" value="${vFirstName!''}" required></input>
                        </div>
                        <div class="form-group">
                            <TD>Last Name</TD>
                            <input type="text" class="form-control" name="vLastName" id="vLastName" placeholder="Last Name" value="${vLastName!''}" required></input>
                        </div>
                        <div class="form-group">
                            <TD>Email Address</TD>
                            <input type="email" class="form-control" name="vEmail" id="vEmail" placeholder="Email Address" value="${vEmail!''}" required></input>
                        </div>
                        <div class="form-group">
                            <TD>Country Code</TD>
                            <input type="text" class="form-control" name="vCountryCode" id="vCountryCode" placeholder="11252" value="${vCountryCode!''}" required></input>
                        </div>
                        <div class="form-group">
                            <TD>Gender</TD>
                            <select value="${vGender!'F'}" id="vGender" name="vGender" class="form-control" data-required="true">
                                <option value="M">M</option>
                                <option value="F" selected="selected">F</option>
                            </select>
                        </div>
                        <div class="form-group" id="showPassWord">
                            <TD>PassWord</TD>
                            <input type="PassWord" class="form-control" name="vEncrtedPassword" id="vEncrtedPassword" placeholder="aa12345" value="${vEncrtedPassword!''}" required></input>
                        </div>
                        <input id="vUserId" name="vUserId" type="hidden" value="">

                        <button type="button" id="newSubmit" class="btn btn-primary" onclick="javascript:doAddUser();">Submit</button>
                        <button type="button" id="editSubmit" class="btn btn-primary" style="display: none;" onclick="javascript:doEditUser();">Update</button>
                    </form>
                </div>
            </div>
            <tr>
              <td>
                  <button type="submit" class="btn btn-primary"  onclick="javascript:doQuery();">Query</button> &nbsp;&nbsp;
                  <button type="button" class="btn btn-primary" onclick="javascript:doOpenAdd();">Add</button>

              </td>
            </tr>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>User Name</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Country Code</th>
                                <th>Gender</th>
                                <th>Edit</th>
                            </tr>
                        </thead>

                        <body>
                            <form action="/deleteUser" id="deleteUser" method="POST" role="form" th:object="${user.vUserId}">
                                <#list users as item>
                                    <tr>
                                        <td>${item?counter}</td>
                                        <td>${item.vUserName}</td>
                                        <td>${item.vFirstName}</td>
                                        <td>${item.vLastName}</td>
                                        <td>${item.vEmail}</td>
                                        <td>${item.vCountryCode}</td>
                                        <td>${item.vGender}</td>
                                        <td>
                                            <button type="button" class="btn btn-primary" onclick="javascript:doDelete('${item.vUserId}','${item.vUserName}');">Delete</button>
                                            <button type="button" class="btn btn-primary" onclick="javascript:doEdit(
                                    '${item.vUserId}','${item.vUserName}','${item.vFirstName}',
                                    '${item.vLastName}','${item.vEmail}','${item.vCountryCode}',
                                    '${item.vGender}');">Edit</button>
                                        </td>
                                    </tr>
                                </#list>
                            </form>
                        </body>
                    </table>
                </div>
            </div>
        </div>
    </div>

</body>

</html>