<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8"></meta>

    <title>Title</title>

    <script
            src="https://code.jquery.com/jquery-3.2.1.js"
            integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
            crossorigin="anonymous"></script>

    <!-- Latest compiled and minified CSS & JS -->
    <link rel="stylesheet" media="screen" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"></link>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
<div class="jumbotron">
    <div class="container">
        <h1>Accounts Manager</h1>
        <p>Demonstrates Reading/Writing from Database using Spring JDBC Template and Kotlin</p>
        <h4>本機：${host!''}</h4>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <form action="@{/}" method="post" role="form" th:object="${user}">
                    <legend>Insert User</legend>

                    <div class="form-group">
                        <label for="UserName">User Name</label>
                        <input type="text" class="form-control" name="vUserName"
                               id="vUserName" placeholder="Use rName" value="${vUserName!''}"></input>
                    </div>

                    <div class="form-group">
                        <label for="FirstName">First Name</label>
                        <input type="text" class="form-control" name="vFirstName"
                               id="vFirstName" placeholder="First Name" value="${vFirstName!''}"></input>
                    </div>

                    <div class="form-group">
                        <label for="LastName">Last Name</label>
                        <input type="text" class="form-control" name="vLastName"
                               id="vLastName" placeholder="Last Name" value="${vLastName!''}"></input>
                    </div>

                    <div class="form-group">
                        <label for="Email">Email Address</label>
                        <input type="text" class="form-control" name="vEmail"
                               id="vEmail" placeholder="Email Address" value="${vEmail!''}"></input>
                    </div>

                    <div class="form-group">
                        <label for="CountryCode">COUNTRYCODE</label>
                        <input type="text" class="form-control" name="vCountryCode"
                               id="vCountryCode" placeholder="11252" value="${vCountryCode!''}"></input>
                    </div>
                    <div class="form-group">
                        <label for="Gender">GENDER</label>
                            <select value="${vGender!'F'}" class="form-control"  data-required="true">
                                <option value="M"  >M</option>
                                <option value="F" selected="selected" >F</option>
                                
                            </select>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table class="table table-striped table-hover" >
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>User Name</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Country Code</th>
                        <th>Gender</th>
                        <th>Edit</th>
                    </tr>
                    </thead>
                    <body>
                        <#list ['a', 'b', 'c'] as i>
                            <tr>
                                <td> ${i?counter}:</td>
                                <td> ${i}</td>
                           </tr>
                        </#list>
                        <#list users as item>
                            <tr>
                                <td>${item?counter}</td>
                                <td>${item.vUserName}</td>
                                <td>${item.vFirstName}</td>
                                <td>${item.vLastName}</td>
                                <td>${item.vEmail}</td>
                                <td>${item.vCountryCode}</td>
                                <td>${item.vGender}</td>
                
                                <td>
                                    <form th:action="@{/}" method="DELETE" role="form" th:field="${item.vUserId}">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    </form> 
                               </td>
                            </tr>
                        </#list>
                        

                    </body>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>